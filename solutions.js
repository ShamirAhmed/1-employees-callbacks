const fs = require('fs');

function readData() {
    const promise = new Promise((resolve, reject) => {
        fs.readFile('./data.json', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

    return promise;
}

function writeFile(fileName, data) {
    const promise = new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(fileName);
            }
        });
    });

    return promise;
}

function dataForIds() {
    readData().then((data) => {
        const dataToJson = JSON.parse(data);
        const employees = dataToJson.employees;

        const ids = employees.filter((employee) => {
            const id2 = employee.id === 2;
            const id13 = employee.id === 13;
            const id23 = employee.id === 23;

            return id2 || id13 || id23;
        });

        return writeFile('problem1.json', JSON.stringify(ids));
    })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        });
}

function groupData() {
    readData().then((data) => {
        const dataToJson = JSON.parse(data);
        const employees = dataToJson.employees;

        const company = employees.reduce((acc, curr) => {
            let ids = {
                id: curr.id,
                name: curr.name
            }
            acc[curr.company].push(ids);
            return acc;
        }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] });

        return writeFile('problem2.json', JSON.stringify(company));
    })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        })
}

function powerpuffBrigade() {
    readData().then((data) => {
        const dataToJson = JSON.parse(data);
        const employees = dataToJson.employees;

        const ids = employees.filter((employee) => {
            return employee.company === 'Powerpuff Brigade'
        });

        return writeFile('problem3.json', JSON.stringify(ids));
    })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        });
}

function removeEntry(){
    readData().then((data) => {
        const dataToJson = JSON.parse(data);
        const employees = dataToJson.employees;

        const ids = employees.filter((employee) => {
            return employee.id !== 2;
        });

        const newEmployees = {
            "employees": ids
        };
        return writeFile('problem4.json', JSON.stringify(newEmployees));
    })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        });
}

removeEntry();